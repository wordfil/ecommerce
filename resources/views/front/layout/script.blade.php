<!-- /#top-banner-and-menu -->
<script src="{{URL::asset('assets/')}}/js/jquery-1.11.1.min.js"></script>
<script src="{{URL::asset('assets/')}}/js/bootstrap.min.js"></script>
<script src="{{URL::asset('assets/')}}/js/bootstrap-hover-dropdown.min.js"></script>
<script src="{{URL::asset('assets/')}}/js/owl.carousel.min.js"></script>
<script src="{{URL::asset('assets/')}}/js/echo.min.js"></script>
<script src="{{URL::asset('assets/')}}/js/jquery.easing-1.3.min.js"></script>
<script src="{{URL::asset('assets/')}}/js/bootstrap-slider.min.js"></script>
<script src="{{URL::asset('assets/')}}/js/jquery.rateit.min.js"></script>
<script type="text/javascript" src="{{URL::asset('assets/')}}/js/lightbox.min.js"></script>
<script src="{{URL::asset('assets/')}}/js/bootstrap-select.min.js"></script>
<script src="{{URL::asset('assets/')}}/js/wow.min.js"></script>
<script src="{{URL::asset('assets/')}}/js/scripts.js"></script>
