<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<title>{{ config('app.name', 'Laravel') }}</title>

<link rel="shortcut icon" type="image/x-icon" href="{{URL::asset('assets/')}}/images/favicon.png">


<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="{{URL::asset('assets/')}}/css/bootstrap.min.css">

<!-- Customizable CSS -->
<link rel="stylesheet" href="{{URL::asset('assets/')}}/css/custom/style.css">
<link rel="stylesheet" href="{{URL::asset('assets/')}}/css/custom/media.css">
<link rel="stylesheet" href="{{URL::asset('assets/')}}/css/main.css">
<link rel="stylesheet" href="{{URL::asset('assets/')}}/css/blue.css">
<link rel="stylesheet" href="{{URL::asset('assets/')}}/css/owl.carousel.css">
<link rel="stylesheet" href="{{URL::asset('assets/')}}/css/owl.transitions.css">
<link rel="stylesheet" href="{{URL::asset('assets/')}}/css/animate.min.css">
<link rel="stylesheet" href="{{URL::asset('assets/')}}/css/rateit.css">
<link rel="stylesheet" href="{{URL::asset('assets/')}}/css/bootstrap-select.min.css">

<!-- Icons/Glyphs -->
<link rel="stylesheet" href="{{URL::asset('assets/')}}/css/font-awesome.css">

<!-- Fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>

