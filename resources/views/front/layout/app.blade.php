<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('front.layout.head')
</head>
<body>
<div class="main-wrapper">

    @include('front.layout.header')

    @include('front.layout.ajax')
    @yield('content')
    @include('front.layout.footer')
    @include('front.layout.dialog')
</div>
@include('front.layout.script')
</body>
</html>
